const yaml = require('js-yaml');
const fs = require('fs');

// Get document, or throw exception on error
try {
    const doc = yaml.safeLoad(fs.readFileSync('.releaserc.yml', 'utf8'));
    console.log(doc);
} catch (e) {
    console.log(e);
}