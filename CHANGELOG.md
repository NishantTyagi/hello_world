## [1.1.1](https://gitlab.com/NishantTyagi/hello_world/compare/v1.1.0...v1.1.1) (2020-11-12)


### Bug Fixes

* random commit ([c42770c](https://gitlab.com/NishantTyagi/hello_world/commit/c42770c11e51926dc6e7a405d38e7c5c57425daa))

# [1.1.0](https://gitlab.com/NishantTyagi/hello_world/compare/v1.0.1...v1.1.0) (2020-11-12)


### Bug Fixes

* yml to js conversion ([0c4d98a](https://gitlab.com/NishantTyagi/hello_world/commit/0c4d98a83ddf37216514c6c06897fee8b92fbc10))


### Features

* logging changes ([e65dba2](https://gitlab.com/NishantTyagi/hello_world/commit/e65dba2c79a5143b23db11f2f0903b5dd7679866))
* **Added sayHello:** sayHello function added to greet user ([5196edf](https://gitlab.com/NishantTyagi/hello_world/commit/5196edfcff4d22193266f9c23ff33725dfd5db38))

# [1.1.0](https://gitlab.com/NishantTyagi/hello_world/compare/v1.0.1...v1.1.0) (2020-11-12)


### Bug Fixes

* yml to js conversion ([0c4d98a](https://gitlab.com/NishantTyagi/hello_world/commit/0c4d98a83ddf37216514c6c06897fee8b92fbc10))


### Features

* **Added sayHello:** sayHello function added to greet user ([5196edf](https://gitlab.com/NishantTyagi/hello_world/commit/5196edfcff4d22193266f9c23ff33725dfd5db38))

# [1.1.0](https://gitlab.com/NishantTyagi/hello_world/compare/v1.0.1...v1.1.0) (2020-11-09)


### Features

* **Added sayHello:** sayHello function added to greet user ([5196edf](https://gitlab.com/NishantTyagi/hello_world/commit/5196edfcff4d22193266f9c23ff33725dfd5db38))

## [1.0.1](https://gitlab.com/NishantTyagi/hello_world/compare/v1.0.0...v1.0.1) (2020-11-09)


### Bug Fixes

* Added greeting statements ([d2695bc](https://gitlab.com/NishantTyagi/hello_world/commit/d2695bc30196f42ef10143e2ada82f64cc31bfcd))
* Added greeting statements ([b7d215b](https://gitlab.com/NishantTyagi/hello_world/commit/b7d215be0f5e18b2025bb69fd7cf3c2e0e48db70))

# 1.0.0 (2020-11-07)


### Bug Fixes

* added token ([dec2c4b](https://gitlab.com/NishantTyagi/hello_world/commit/dec2c4bc1383e4f461236979e145f44ecfc2e71b))


### Features

* **added ci config:** CI configuration added ([1d5c0ca](https://gitlab.com/NishantTyagi/hello_world/commit/1d5c0caacf156ccf1a58483b704717d60fc9ca43))
